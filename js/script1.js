function selectButton(btn) {
    var buttons = document.getElementsByClassName('btn');
    for (var i = 0; i < buttons.length; i++) {
      if (buttons[i] !== btn || !btn.classList.contains('selected')) {
        buttons[i].classList.remove('selected');
      }
    }
    
    if (!btn.classList.contains('selected')) {
      btn.classList.add('selected');
    }
  }
  
  window.onload = function() {
    var selectAllButton = document.querySelector('.btn');
    selectButton(selectAllButton);
  };
  