// JavaScript
document.addEventListener("DOMContentLoaded", function() {
    var entrarLink = document.getElementById("entrarLink");
    var menuDesplegable = document.getElementById("menuDesplegable");

    entrarLink.addEventListener("click", function(event) {
        event.preventDefault();
        menuDesplegable.classList.toggle("hidden");
        entrarLink.classList.toggle("bg-red-600");
        entrarLink.classList.toggle("bg-gray-300");
        entrarLink.classList.toggle("text-white");
    });

    document.addEventListener("click", function(event) {
        if (!menuDesplegable.contains(event.target) && event.target !== entrarLink) {
            menuDesplegable.classList.add("hidden");
            entrarLink.classList.remove("bg-gray-300");
            entrarLink.classList.add("bg-red-600");
            entrarLink.classList.add("text-white");
        }
    });

    var opcionesMenu = menuDesplegable.querySelectorAll("a");
    opcionesMenu.forEach(function(opcion) {
        opcion.addEventListener("mouseover", function() {
            opcion.classList.add("hover:bg-gray-200");
        });
        opcion.addEventListener("mouseout", function() {
            opcion.classList.remove("hover:bg-gray-200");
        });
    });
});


document.getElementById('closeBtn').addEventListener('click', function() {
    document.getElementById('infoDiv').style.display = 'none';
});