function mostrarVentana() {
    // Obtiene el elemento con el id "ventana-ayuda"
    var ventana = document.getElementById("ventana-ayuda");
    // Cambia el estilo de display a block para hacerlo visible
    ventana.style.display = "block";
}

// Función para cerrar la ventana de ayuda
function cerrarVentana() {
    // Obtiene el elemento con el id "ventana-ayuda"
    var ventana = document.getElementById("ventana-ayuda");
    // Cambia el estilo de display a none para ocultarlo
    ventana.style.display = "none";
}
