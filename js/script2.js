let currentIndex = 0;
const carousel = document.querySelector('.carousel');
const numCards = carousel.children.length;
const cardWidth = carousel.querySelector('.card').offsetWidth;

function moveRight() {



  if (currentIndex < numCards - 3) {
    document.querySelector('.prev').style.backgroundColor = 'black'; // Cambiar el color de fondo de la flecha izquierda
    currentIndex++;
    // const offset = -currentIndex * cardWidth;
    const offset = Math.min(-currentIndex * cardWidth, (numCards - 3) * cardWidth);
    carousel.style.transform = `translateX(${offset-100}px)`;
  }

  

  if (currentIndex == numCards - 3) {
    document.querySelector('.next').style.backgroundColor = 'grey'; // Cambiar el color de fondo de la flecha izquierda

  }


}

function moveLeft() {
  if (currentIndex > 0) {
    document.querySelector('.next').style.backgroundColor = 'black'; // Cambiar el color de fondo de la flecha izquierda

    currentIndex--;
    const offset = -currentIndex * cardWidth;
    carousel.style.transform = `translateX(${offset}px)`;
  }

  if (currentIndex == 0) {
    document.querySelector('.prev').style.backgroundColor = 'grey'; // Cambiar el color de fondo de la flecha izquierda

  }


}
